import 'dart:io';

extension ListExt<E> on List<E> {
  E? get(int index) {
    if (index < 0 || index >= length) return null;
    return this[index];
  }

  List<E> set(int index, E value) {
    if (index >= 0 && index < length) {
      this[index] = value;
    }

    return this;
  }
}

extension Matrix2 on List<List<int>> {
  bool _isLower(int? n, [int compare = 0]) =>
      n != null && n >= 0 && n < compare;

  List<List<int>> applyLuminance({
    List<List<int>>? copy,
    int x = 0,
    int y = 0,
  }) {
    final matrix = copy ??
        [
          ...map((e) => [...e])
        ];

    if (x == matrix.length - 1 && y == matrix.length - 1) return matrix;

    final row = matrix.get(y);
    final col = row?.get(x);

    if (col == null) {
      return applyLuminance(copy: matrix, y: y + 1);
    }

    if (col == 0) {
      return applyLuminance(copy: matrix, x: x + 1, y: y);
    }

    final prev = x - 1;
    final next = x + 1;

    final light = col - 1;

    final left = row?.get(prev);
    final right = row?.get(next);

    final above = matrix.get(y - 1);
    final below = matrix.get(y + 1);

    final top = above?.get(x);
    final bottom = below?.get(x);

    if (_isLower(right, light)) row?.set(next, light);

    if (_isLower(bottom, light)) below?.set(x, light);

    if (_isLower(top, light)) {
      above!.set(x, light);

      return applyLuminance(copy: matrix, x: x, y: y - 1);
    }

    if (_isLower(left, light)) {
      row?.set(prev, light);

      return applyLuminance(copy: matrix, x: prev, y: y);
    }

    return applyLuminance(copy: matrix, x: next, y: y);
  }
}

void main() {
  final cases = <List<List<int>>>[
    [
      [0, 0, 4, 0],
      [0, 0, 0, 0],
      [0, 2, 0, 0],
      [0, 0, 0, 0]
    ],
    [
      [2, 0, 0, 3],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0]
    ],
    [
      [2, 0, 0, 3],
      [0, 0, 0, 0],
      [0, 0, 15, 0],
      [10, 0, 0, 0]
    ],
  ];

  for (final input in cases) {
    final output = input.applyLuminance();

    _print(output);
  }
}

void _print(List<List<int>> matrix) {
  for (var i = 0; i < matrix.length; i++) {
    for (var j = 0; j < matrix[i].length; j++) {
      stdout.write('${matrix[i][j]} ');
    }

    stdout.write('\n');
  }
}
