/**
 * Definition for a binary tree node.
 * class TreeNode {
 *   int val;
 *   TreeNode? left;
 *   TreeNode? right;
 *   TreeNode([this.val = 0, this.left, this.right]);
 * }
 */
class Solution {
  int deepestLeavesSum(TreeNode? root) {
    final List<TreeNode> deepestLeaves = _getDeepestLeaves(root!);

    return deepestLeaves
        .map((e) => e.val)
        .reduce((value, element) => value + element);
  }

  // This is not being used but this was an implementation helped me
  // to find the "right direction" of the final solution.
  List<TreeNode> _getDepthFreeNodes(TreeNode node, {int depth = 0}) {
    final Map<int, List<TreeNode>> leaves = _getLeaves(node, depth: depth);

    return leaves.values.reduce((value, element) => [...value, ...element]);
  }

  List<TreeNode> _getDeepestLeaves(TreeNode node, {int depth = 0}) {
    final Map<int, List<TreeNode>> leaves = _getLeaves(node, depth: depth);

    final List<int> levels = leaves.keys.toList()..sort((a, z) => z - a);

    final int deepestLevel = levels.first;

    final List<TreeNode> deepestLeaves = leaves[deepestLevel]!;

    return deepestLeaves;
  }

  Map<int, List<TreeNode>> _getLeaves(
    TreeNode node, {
    Map<int, List<TreeNode>> treeByDepth = const {},
    int depth = 0,
  }) {
    // Copy to avoid object reference related bugs.
    Map<int, List<TreeNode>> copy = Map.from(treeByDepth);

    if (node.left == null && node.right == null) {
      copy[depth] = [...(copy[depth] ?? []), node];
    } else {
      if (node.left != null) {
        copy = _getLeaves(node.left!, treeByDepth: copy, depth: depth + 1);
      }
      if (node.right != null) {
        copy = _getLeaves(node.right!, treeByDepth: copy, depth: depth + 1);
      }
    }

    return copy;
  }
}
