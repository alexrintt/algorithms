import './permutation.dart';

void main() {
  print([1, 2].permutation()); // [[1, 2], [2, 1]].
  print([1, 2].permutation().length); // 2.

  print([1].permutation()); // [1].
  print([1].permutation().length); // 1.

  print([1, 2, 3].permutation()); // ...
  print([1, 2, 3].permutation().length); // 6.

  print([1, 2, 3, 4].permutation()); // ...
  print([1, 2, 3, 4].permutation().length); // 24.
}
