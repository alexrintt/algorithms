## Problem

Given an array of N elements, create a tree that can represents all it's permutations.

I was searching for an solution but I did found this site: https://www.baeldung.com/cs/array-generate-all-permutations.

I did not use the text solution but this image:

![image](https://user-images.githubusercontent.com/51419598/200136668-524795ce-cefc-42e3-8c18-9a953bc8b570.png)

I thought the image was so good that would be a waste if I looked at the text solution lol.

So I tried to implement this binary tree using Dart, and surprisingly it worked.

```dart
void main() {
  print([1, 2].permutation()); // [[1, 2], [2, 1]].
  print([1, 2].permutation().length); // 2.

  print([1].permutation()); // [1].
  print([1].permutation().length); // 1.

  print([1, 2, 3].permutation()); // ...
  print([1, 2, 3].permutation().length); // 6.

  print([1, 2, 3, 4].permutation()); // ...
  print([1, 2, 3, 4].permutation().length); // 24.
}
```

## Context

I was creating a simple system of vectors in Dart, where wanted to sum, subtract, multiply, and do all kind of operations between `Vector2`s.

Then suddently I was writing a test to verify if a given array of `Vector2` produces the same result when the sum was done through a different order, like:

```dart
class Vector2 {
  // My implementation.
}

void main() {
  final List<Vector2> vectors = [Vector2(...), Vector2(...), Vector2(...)];
  vectors.forEachPossibleOrder((order) => ...);
  // How am I going to find the [forEachPossibleOrder] ???
}

```
