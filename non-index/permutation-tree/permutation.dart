class Node<T> {
  final Node<T>? parent;
  final List<Node<T>> children;
  final T value;

  int get depth => getTopDownParents().length;

  void transverse(void Function(Node<T>) fn) {
    return _transverseRecusively(this, fn);
  }

  List<Node<T>> getTopDownParents() {
    return _getTopDownParentsRecursively(this);
  }

  List<Node<T>> getDownTopParents() {
    return getTopDownParents().reversed.toList();
  }

  List<Node<T>> _getTopDownParentsRecursively(Node<T> node) {
    if (node.parent == null) return [];
    return [..._getTopDownParentsRecursively(node.parent!), node.parent!];
  }

  int nodesCount({bool includeItself = true}) {
    int count = 0;

    transverse((_) => count++);

    return includeItself ? count : count - 1;
  }

  List<Node<T>> getLeaves() {
    List<Node<T>> leaves = [];

    transverse((node) {
      if (node.children.isEmpty) {
        leaves.add(node);
      }
    });

    return leaves;
  }

  void _transverseRecusively(Node<T> node, void Function(Node<T>) fn) {
    fn(node);
    for (final Node<T> child in node.children) _transverseRecusively(child, fn);
  }

  const Node({
    this.parent,
    required this.children,
    required this.value,
  });
}

extension Permutation<T> on Iterable<T> {
  Node<List<T>> _generatePermutationTreeOf({
    Node<List<T>>? parent,
    required List<T> combination,
  }) {
    final int depth = parent == null ? 0 : parent.depth + 1;
    final int? fixedIndex = depth == 0 ? null : depth - 1;

    final bool hasFixed = fixedIndex != null;

    final List<T> fixed =
        hasFixed ? combination.take(fixedIndex + 1).toList() : <T>[];

    final List<T> unpermuted =
        hasFixed ? combination.skip(fixedIndex + 1).toList() : combination;

    final int lastIndex = combination.length - 1;

    final Node<List<T>> node = Node<List<T>>(
      value: combination,
      parent: parent,
      children: <Node<List<T>>>[],
    );

    if (lastIndex - 1 == fixedIndex) {
      return node;
    } else {
      final Iterable<Node<List<T>>> children = [
        for (var i = 0; i < unpermuted.length; i++)
          _generatePermutationTreeOf(
            combination: [
              ...fixed,
              unpermuted.elementAt(i),
              ...unpermuted.take(i),
              ...unpermuted.skip(i + 1)
            ],
            parent: node,
          ),
      ];

      return node..children.addAll(children);
    }
  }

  Iterable<Iterable<T>> permutation({Node? parent}) {
    final Node<Iterable<T>> tree = permutationTree();
    return tree.getLeaves().map((e) => e.value);
  }

  Node<Iterable<T>> permutationTree() {
    return _generatePermutationTreeOf(combination: this.toList());
  }

  int permutationCount() {
    return permutationTree().getLeaves().length;
  }
}
