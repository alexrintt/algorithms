const todec = (bin) =>
  bin
    .split("")
    .map((d, i, arr) => (d === "1") * (1 << (arr.length - 1 - i)))
    .reduce((total, e) => total + e, 0);

todec('11111111'); // 255
todec('1011'); // 11
todec('1'); // 1
todec('10'); // 2
