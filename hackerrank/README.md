# 🍂 Hacker Rank Solutions

Este é um repositório com várias resoluções do site [Hacker Rank](https://www.hackerrank.com/), sinta-se livre para usar como estudo ou se precisar, só me chamar no Discord para tirar qualquer dúvida :)

## Suporte

Se tiver ideias pra compartilhar, bugs pra reportar ou precisar de suporte, você pode abrir uma issue ou entrar no [servidor do Discord](https://discord.gg/86GDERXZNS).

## Folder structure

- This repository follows:

```
.
└── <programming-language>
    └── <difficult>
        └── <problem-slug>.<lang-ext>
```

- Example:

```
.
└── typescript
    └── easy
        └── counting-valleys.ts
```
