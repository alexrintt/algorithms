# 📜 Project Euler Solutions

Este é um repositório com várias resoluções dos arquivos do site [Project Euler](https://projecteuler.net/), sinta-se livre para usar como estudo ou se precisar, só me chamar no Discord para tirar qualquer dúvida :)

## Suporte

Se tiver ideias pra compartilhar, bugs pra reportar ou precisar de suporte, você pode abrir uma issue ou entrar no servidor do Discord

<a href="https://discord.gg/86GDERXZNS">
  <kbd><img src="https://discordapp.com/api/guilds/771498135188799500/widget.png?style=banner2" alt="Discord Banner"/></kbd>
</a>

## Folder structure

- This repository follows:

```
.
└── <programming-language>
    └── <problem-id>-<problem-slug>.<lang-ext>
```

- Example:

```
.
└── python3
    └── 1-multiples-of-3-and-5.py
```
